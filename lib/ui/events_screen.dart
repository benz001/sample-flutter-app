import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:intl/intl.dart';
import 'package:sample_flutter_app/parameter_screen.dart';
import 'package:sample_flutter_app/ui/choose_screen.dart';


class EventsScreen extends StatefulWidget {
  static const String routeName = "/eventsScreen";
  const EventsScreen({Key key}) : super(key: key);

  @override
  _EventsScreenState createState() => _EventsScreenState();
}

class _EventsScreenState extends State<EventsScreen> {

  final String apiUrl = "https://reqres.in/api/users?per_page=15";

  Future<List<dynamic>> _fecthDataUsers() async {
    var result = await http.get(apiUrl);
    return json.decode(result.body)['data'];
  }

  String _currentDate() {
    final DateTime now = DateTime.now();
    final DateFormat formatter = DateFormat('dd-MM-yyyy');
    final String formatted = formatter.format(now);
    // print(formatted);
    return formatted;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("EVENTS"),
          backgroundColor: Color.fromRGBO(231, 120, 45, 1),
          actions: [
            IconButton(
                icon: Image.asset("images/ic_search_white.png"), onPressed: () {}),
            IconButton(
                icon: Image.asset("images/ic_map_view.png"), onPressed: () {}),
          ],
        ),
        body: Container(
          child: FutureBuilder(
            future: _fecthDataUsers(),
            builder: (BuildContext context, AsyncSnapshot snapshot) {
              if (snapshot.hasData) {
                return ListView.builder(
                    padding: EdgeInsets.only(left: 15, right: 15),
                    itemCount: snapshot.data.length,
                    itemBuilder: (BuildContext context, int index) {
                      return GestureDetector(
                        onTap: () {
                           Navigator.pushNamed(
                                    context,
                                    ChooseScreen.routeName,
                                    arguments:
                                        ParameterScreen(snapshot.data[index]["id"].toString(),"Choose Guest"),
                                  );
                        },
                        child: Card(
                          elevation: 5,
                          margin: EdgeInsets.only(top: 10),
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10)),
                          child: Row(
                            children: <Widget>[
                              Expanded(
                                flex: 5,
                                child: Container(
                                  height: 150,
                                  width: 100,
                                  decoration: BoxDecoration(
                                      color: Colors.grey,
                                      borderRadius: BorderRadius.only(
                                          topLeft: Radius.circular(10),
                                          bottomLeft: Radius.circular(10)),
                                      image: DecorationImage(
                                        image: NetworkImage(
                                            snapshot.data[index]["avatar"]),
                                        fit: BoxFit.cover,
                                      )),
                                  // child: Image.asset("images/naruto.jpg", fit: BoxFit.fill,),
                                ),
                              ),
                              Expanded(
                                  flex: 5,
                                  child: Container(
                                    // color: Colors.red,
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: <Widget>[
                                        Text(snapshot.data[index]["id"]
                                            .toString()),
                                        Text(snapshot.data[index]
                                                ["first_name"] +
                                            " " +
                                            (snapshot.data[index]
                                                ["last_name"])),
                                        Text(
                                          _currentDate(),
                                          textAlign: TextAlign.right,
                                        )
                                      ],
                                    ),
                                  ))
                            ],
                          ),
                        ),
                      );
                    });
              } else {
                return Center(
                  child: CircularProgressIndicator(),
                );
              }
            },
          ),
        )
        // ListView(
        //   children: <Widget>[
        //     Padding(
        //       padding: EdgeInsets.only(left: 15, right: 15),
        //       child: Card(
        //         elevation: 5,
        //         shape: RoundedRectangleBorder(
        //             borderRadius: BorderRadius.circular(10)),
        //         margin: EdgeInsets.only(top: 10),
        //         child: Row(
        //           children: <Widget>[
        //             Expanded(
        //               flex: 5,
        //               child: Container(
        //                 height: 150,
        //                 width: 100,
        //                 decoration: BoxDecoration(
        //                     color: Colors.red,
        //                     borderRadius: BorderRadius.only(
        //                         topLeft: Radius.circular(10),
        //                         bottomLeft: Radius.circular(10)),
        //                     image: DecorationImage(
        //                       image: AssetImage("images/naruto.jpg"),
        //                       fit: BoxFit.cover,
        //                     )),
        //                 // child: Image.asset("images/naruto.jpg", fit: BoxFit.fill,),
        //               ),
        //             ),
        //             Expanded(
        //                 flex: 5,
        //                 child: Container(
        //                   color: Colors.red,
        //                   child: Column(
        //                     crossAxisAlignment: CrossAxisAlignment.start,
        //                     children: <Widget>[
        //                       Text("Title"),
        //                       Text("Content"),
        //                       Text(
        //                         "Date",
        //                         textAlign: TextAlign.right,
        //                       )
        //                     ],
        //                   ),
        //                 ))
        //           ],
        //         ),
        //       ),
        //     )
        //   ],
        // ),
        );
  }
}
