import 'package:flutter/material.dart';
import 'package:sample_flutter_app/parameter_screen.dart';
import 'package:sample_flutter_app/ui/choose_screen.dart';
import 'package:shared_preferences/shared_preferences.dart';

class WelcomeScreen extends StatefulWidget {
  const WelcomeScreen({Key key}) : super(key: key);

  @override
  _WelcomeScreenState createState() => _WelcomeScreenState();
}

class _WelcomeScreenState extends State<WelcomeScreen> {
  TextEditingController _controllerName = TextEditingController();

  _saveName(BuildContext context) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var _name = _controllerName.text;
    var _guestName = "";
    await prefs.setString("name", _name);
    await prefs.setString("guest_name", _guestName);
    // Navigator.pushNamed(context, ChooseScreen.routeName);
    Navigator.pushNamed(
      context,
      ChooseScreen.routeName,
      arguments: ParameterScreen("","Choose Guest"),
    );
  }

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;
    return Scaffold(
      resizeToAvoidBottomPadding: false,
      body: Stack(
        children: <Widget>[
          Container(
            color: Colors.grey,
            width: double.infinity,
            height: double.infinity,
            child: Column(
              children: <Widget>[
                Container(
                  width: double.infinity,
                  height: height * 0.35,
                  decoration: BoxDecoration(
                    color: Color.fromRGBO(205, 134, 87, 1),
                    image: DecorationImage(
                      image: AssetImage("images/bg_bright.png"),
                      fit: BoxFit.cover,
                    ),
                  ),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Text(
                        "Welcome",
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 18,
                            fontWeight: FontWeight.bold),
                      ),
                      Text("This is app for suitmedia mobile developer test",
                          style: TextStyle(color: Colors.white))
                    ],
                  ),
                ),
                Container(
                  width: double.infinity,
                  height: height * 0.65,
                  color: Colors.white,
                  alignment: Alignment.bottomCenter,
                  padding: EdgeInsets.only(bottom: 15),
                  child: Text(
                    "Copyright @2020 All rights reserved.",
                    style: TextStyle(
                      color: Color.fromRGBO(195, 195, 195, 1),
                    ),
                  ),
                )
              ],
            ),
          ),
          Center(
            child: Container(
              width: width * 0.8,
              height: height * 0.5,
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(15),
                border: Border.all(
                    color: Colors.white, style: BorderStyle.solid, width: 1),
                boxShadow: [
                  BoxShadow(
                    color: Colors.grey.withOpacity(0.5),
                    spreadRadius: 2,
                    blurRadius: 7,
                    offset: Offset(0, 3), // changes position of shadow
                  ),
                ],
              ),
              padding: EdgeInsets.fromLTRB(15, 15, 15, 0),
              child: Column(
                children: <Widget>[
                  Image.asset(
                    "images/img_avatar.png",
                    height: 100,
                  ),
                  Container(
                    decoration: BoxDecoration(
                        color: Color.fromRGBO(231, 231, 231, 1),
                        borderRadius: BorderRadius.circular(30)),
                    margin: EdgeInsets.only(top: 10),
                    child: TextField(
                      controller: _controllerName,
                      textAlign: TextAlign.center,
                      decoration: InputDecoration(
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(30)),
                          hintText: "Type name here...",
                          hintStyle: TextStyle(color: Colors.grey)),
                    ),
                  ),
                  SizedBox(height: 20),
                  ButtonTheme(
                    minWidth: width,
                    height: 50,
                    child: RaisedButton(
                        child: Text(
                          "Next",
                          style: TextStyle(color: Colors.white),
                        ),
                        color: Color.fromRGBO(231, 120, 45, 1),
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(20)),
                        onPressed: () {
                          _saveName(context);
                        }),
                  )
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}
