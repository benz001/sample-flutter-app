import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:sample_flutter_app/parameter_screen.dart';
import 'package:sample_flutter_app/ui/choose_screen.dart';
import 'package:shared_preferences/shared_preferences.dart';

class GuestsScreen extends StatefulWidget {
  static const String routeName = "/guestsScreen";
  const GuestsScreen({Key key}) : super(key: key);

  @override
  _GuestsScreenState createState() => _GuestsScreenState();
}

class _GuestsScreenState extends State<GuestsScreen> {
  static const String routeName = "/guestsScreen";
  Future<List<dynamic>> _fecthDataUsers() async {
    final String apiUrl = "https://reqres.in/api/users?per_page=15";
    var result = await http.get(apiUrl);
    return json.decode(result.body)['data'];
  }

  String checkPhoneUser(int id){
    if (id % 2 == 0) {
      return "Blackberry";
    }else if(id % 3 == 0){
       return "Android";
    }else if(id % 2 == 0 && id % 3 == 0){
       return "iOS";
    }else{
      return "iOS";
    }
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("GUESTS"),
          backgroundColor: Color.fromRGBO(231, 120, 45, 1),
        ),
        body: Container(
          margin: EdgeInsets.all(10),
          child: Container(
            child: FutureBuilder(
              future: _fecthDataUsers(),
              builder: (BuildContext context, AsyncSnapshot snapshot) {
                if (snapshot.hasData) {
                  return GridView.builder(
                      itemCount: snapshot.data.length,
                      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                          crossAxisCount: 2,
                          mainAxisSpacing: 3,
                          crossAxisSpacing: 3,
                          childAspectRatio: 1),
                      itemBuilder: (BuildContext context, int index) {
                        return Container(
                          height: 200,
                          child: Column(
                            children: <Widget>[
                              GestureDetector(
                                onTap: () {
                                  Navigator.pushNamed(
                                    context,
                                    ChooseScreen.routeName,
                                    arguments:
                                        ParameterScreen("",snapshot.data[index]["first_name"]),
                                  );
                                },
                                child: Container(
                                  width: 120,
                                  height: 120,
                                  decoration: BoxDecoration(
                                      color: Colors.grey,
                                      shape: BoxShape.circle,
                                      image: DecorationImage(
                                        image: NetworkImage(
                                            snapshot.data[index]["avatar"]),
                                        fit: BoxFit.cover,
                                      )),
                                ),
                              ),
                              Center(
                                child: Text(
                                  snapshot.data[index]["first_name"]+" "+checkPhoneUser(snapshot.data[index]["id"]),
                                ),
                              )
                            ],
                          ),
                        );
                      });
                } else {
                  return Center(
                    child: CircularProgressIndicator(),
                  );
                }
              },
            ),
          ),
        ));
  }
}
